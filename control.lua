
function OnTick(event)

  if game.tick % 60 ~= 0 then
    return
  end

  local trains_backlog = tonumber(settings.global["trainpause-backlog"].value)

  local stops_by_name = {}

  -- The next station name in a train's schedule (current+1)
  local train_station_next = function(train)
    if train ~= nil then
      local schedule = train.schedule
      if schedule ~= nil and schedule.records ~= nil and #schedule.records > 0 then
        local next_leg = schedule.records[schedule.current+1] or (schedule.current > 1 and schedule.records[1] or nil)
        if next_leg ~= nil then
          return next_leg.station
        end
      end
    end
    return nil
  end

  -- The current station name in a train's schedule (either en-route to, or stopped at)
  local train_station_current = function(train)
    if train ~= nil then
      local schedule = train.schedule
      if schedule ~= nil and schedule.records ~= nil and schedule.records[schedule.current] ~= nil then
        return schedule.records[schedule.current].station
      end
    end
    return nil
  end

  -- A list of trains en-route to, or stopped at, stations of this name
  local station_trains = function(station_name)
    local trains = {}
    if stops_by_name[station_name] ~= nil then
      for i, stop in pairs(stops_by_name[station_name]) do
        local stop_trains = stop.get_train_stop_trains()
        if stop_trains ~= nil then
          for j, train in pairs(stop_trains) do
            if train_station_current(train) == station_name then
              trains[train.id] = train
            end
          end
        end
      end
    end
    local list = {}
    for id, train in pairs(trains) do
      table.insert(list, train)
    end
    return list
  end

  -- If a connected entity is a train stop with a stopped train, peek at the
  -- next station on the train's schedule and emit a green signal if the
  -- station is available, where "avaialble" means one or more stations with
  -- the same name don't have a train en-route or stopped.
  local process_train_stop = function(pauser, stop, wire)
    if not stop.valid or stop.name ~= "train-stop" then
      return
    end
    local trains = stop.get_train_stop_trains()
    if trains == nil then
      return
    end
    for j, train in pairs(trains) do
      if train.station == stop then
        local destination = train_station_next(train)
        if destination ~= nil and stops_by_name[destination] ~= nil then
          local in_play = station_trains(destination)
          if #in_play / #stops_by_name[destination] < trains_backlog then
            pauser.get_control_behavior().parameters = {
              parameters = {
                {
                  index = 1,
                  signal = {
                    type = "virtual",
                    name = "signal-green",
                  },
                  count = 1
                }
              }
            }
          end
        end
        break
      end
    end
  end

  -- Build an table of train stops indexed by backer_name
  for _, surface in pairs(game.surfaces) do
    local stops = surface.find_entities_filtered{type="train-stop"}
    if stops ~= nil then
      for i, stop in pairs(stops) do
        if stop.valid then
          if stops_by_name[stop.backer_name] == nil then
            stops_by_name[stop.backer_name] = {}
          end
          table.insert(stops_by_name[stop.backer_name], stop)
        end
      end
    end
  end

  for id, pauser in pairs(global.TrainPausers) do
    if pauser.valid then
      pauser.get_control_behavior().parameters = nil
      local connected = pauser.circuit_connected_entities
      if connected ~= nil then
        for i, entity in pairs(connected.red) do
          process_train_stop(pauser, entity, "red")
        end
        for i, entity in pairs(connected.green) do
          process_train_stop(pauser, entity, "green")
        end
      end
    end
  end
end

function OnEntityCreated(event)
  if event.created_entity.name == "train-pauser" then
    global.TrainPausers = global.TrainPausers or {}
    local entity = event.created_entity
    entity.operable = false
    global.TrainPausers[entity.unit_number] = entity
  end
end

function OnEntityRemoved(event)
  if event.entity.name == "train-pauser" then
    table.remove(global.TrainPausers, event.entity.unit_number)
  end
end

script.on_init(function()
  log("trainpause on_init")
  global.TrainPausers = global.TrainPausers or {}
  script.on_event(defines.events.on_tick, OnTick)
  script.on_event({defines.events.on_built_entity, defines.events.on_robot_built_entity}, OnEntityCreated)
  script.on_event({defines.events.on_pre_player_mined_item, defines.events.on_robot_pre_mined, defines.events.on_entity_died}, OnEntityRemoved)
end)

script.on_load(function()
  log("trainpause on_load")
  global.TrainPausers = global.TrainPausers or {}
  script.on_event(defines.events.on_tick, OnTick)
  script.on_event({defines.events.on_built_entity, defines.events.on_robot_built_entity}, OnEntityCreated)
  script.on_event({defines.events.on_pre_player_mined_item, defines.events.on_robot_pre_mined, defines.events.on_entity_died}, OnEntityRemoved)
end)
