data:extend({
  {
    type = "item",
    name = "train-pauser",
    icon = "__trainpause__/graphics/icons/train-pauser.png",
    icon_size = 32,
    flags = {"goes-to-main-inventory"},
    subgroup = "circuit-network",
    place_result = "train-pauser",
    order = "c[combinators]-d[train-pauser]",
    stack_size = 50,
  }
})