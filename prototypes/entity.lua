local pauser = table.deepcopy(data.raw["constant-combinator"]["constant-combinator"])
pauser.name = "train-pauser"
pauser.minable.result = "train-pauser"
pauser.icon = "__trainpause__/graphics/icons/train-pauser.png"
pauser.icon_size = 32
data:extend({ pauser })
