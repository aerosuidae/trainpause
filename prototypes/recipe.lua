data:extend({
  {
    type = "recipe",
    name = "train-pauser",
    icon = "__trainpause__/graphics/icons/train-pauser.png",
    icon_size = 32,
    enabled = "false",
    ingredients =
    {
      {"constant-combinator", 1},
      {"advanced-circuit", 1}
    },
    result = "train-pauser"
  }
})