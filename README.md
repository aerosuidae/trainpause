# Factorio Mod: Train Pause

A combinator to pause trains at stations until the next station is available.

Alpha code!

## Intended bahaviour

If an entity connected via circuit to the combinator is a train stop with a parked
train, peek at the the train's schedule and emit a green signal if the next station
is available.

"Available" means: *one or more stations with the next name do not have a train
already en-route or stopped*.

This has only been tested with relatively simple train setups so far so bug reports
welcome...

## Setup

At any station where trains should wait:

* Connect the combinator to the train stop with a circuit wire
* Set trains to wait at the station until circuit condition GREEN > 0
* Set train backlog in mod settings

## Vague plans

* Make the backlog at target stations configurable per station name/stop

## Why?

*How is this different from using a train buffer?*

The test use case is a city blocks base with an approach something like *LTN-in-reverse*. Trains get preloaded with resources and sent to queue at a central depot-style pause station. Delivery stations can be quickly serviced with no need to disable stations, use unique names, cater for long queues and rerouting, run circuit wires everywhere, or wait for resource loading.
