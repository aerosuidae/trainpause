local function get_recipe(name)
  local recipes = data.raw["recipe"]
  if recipes then
    return recipes[name]
  end
  return nil
end

local function get_technology(name)
  local technologies = data.raw["technology"]
  if technologies then
    return technologies[name]
  end
  return nil
end

local function attach_recipe_to_tech(recipe, technology)
  local effects = technology.effects

  if not effects then
    technology.effects = {}
    effects = technology.effects
  end

  local load = true

  for k,v in pairs(effects) do
    if k == "unlock-recipe" then
      if v.recipe.name == recipe.name then
        load = false
      end
    end
  end

  if load then
    table.insert(effects, {type = "unlock-recipe", recipe = recipe.name})
  end
end

local recipe = get_recipe("train-pauser")
local technology = get_technology("automated-rail-transportation")

if recipe ~= nil and technology ~= nil then
  attach_recipe_to_tech(recipe, technology)
else
  log("trainpause: can't attach to tech automated-rail-transportation")
end
