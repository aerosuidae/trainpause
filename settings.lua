data:extend({
  {
    type = "int-setting",
    name = "trainpause-backlog",
    order = "ab",
    setting_type = "runtime-global",
    default_value = 2,
    minimum_value = 1,
    maximum_value = 4294967295, -- prevent 32bit signed overflow
  },
})